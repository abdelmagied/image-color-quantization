﻿using MetroFramework.Controls;
using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace p
{
    public partial class Form2 : MetroForm
    {

        fileoperations operation = new fileoperations();
        binarysearch bs = new binarysearch();
        employee emp = new employee();
        HandleErrors handle = new HandleErrors();
        orderingattribute order = new orderingattribute();
        List<employee> _employee;
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
        }

        private void metroGrid1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
         
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            
        }

        private void metroButton8_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form1 f = new Form1();
            f.Show();
            f.FormClosed += new FormClosedEventHandler(sf_FormClosed);
         }
          void sf_FormClosed(object sender, FormClosedEventArgs e)
          {
            Application.Exit();
          }

          private void metroButton1_Click(object sender, EventArgs e)
          {

              List<employee> _employee = new List<employee>();
              string searchdata = metroTextBox1.Text;
              if (searchdata == "")
              {
                  MessageBox.Show("no entered data to search for"); return;
              }
              // handle.ssn = searchdata;
              //  if (handle.SearchExpectedErrors()) return;
              _employee = operation.readFromXml();
              int index = bs.BS(_employee, searchdata);
              if (index == -1)
              {
                  /*MessageBox.Show("Data not found in the file !!! ");*/
                  groupBox2.Show(); FormClosed +=new FormClosedEventHandler(fs_FormClosed); return; /*sdfadfsdafdsfsaf*/
              }
              if (metroGrid1.ColumnCount == 0)
              {
                  metroGrid1.Columns.Add("Name", "Name");
                  metroGrid1.Columns.Add("SSN", "SSN");
                  {
                      metroGrid1.Columns.Add("Job", "Job");
                      metroGrid1.Columns.Add("Gender", "Gender");
                      metroGrid1.Columns.Add("Salary", "Salary");
                  }
                  metroGrid1.Rows.Add(new string[] { _employee[index].name, _employee[index].ssn, _employee[index].job, _employee[index].gender, _employee[index].salary });
                  metroGrid1.ClearSelection();
              }
              else
              {
                  metroGrid1.Rows.Add(new string[] { _employee[index].name, _employee[index].ssn, _employee[index].job, _employee[index].gender, _employee[index].salary });
                  metroGrid1.ClearSelection();
              }
              Form1 f = new Form1();
              groupBox1.Show(); f.FormClosed += new FormClosedEventHandler(fs_FormClosed);/*sfdf*/
              metroGrid3.Rows.Clear();
          }
          void fs_FormClosed(object sender, FormClosedEventArgs e)
          {
              Application.Exit();
          }

          private void metroButton7_Click(object sender, EventArgs e)
          {
             
              List<employee> _employee = new List<employee>();
              string _name = metroTextBox11.Text;
              string _job = metroTextBox10.Text;
              string _salary = metroTextBox9.Text;
              string _gender = metroTextBox8.Text;
              string _ssn = metroTextBox7.Text;
              if ((order.GetPrimaryKey().ToLower() == "name" && _name == "") || (order.GetPrimaryKey().ToLower() == "job" && _job == "") || (order.GetPrimaryKey().ToLower() == "salary" && _salary == "") || (order.GetPrimaryKey().ToLower() == "ssn" && _ssn == ""))
              {
                  MessageBox.Show("Primary Key " + order.GetPrimaryKey() + " is Required "); return;
              }

              //   handle.name = _name; handle.job = _job; handle.salary = _salary; handle.gender = _gender; handle.ssn = _ssn;
              //    if (handle.UpdataExpectedErrors()) {textBox2.Clear(); textBox3.Clear(); textBox4.Clear(); textBox6.Clear(); return; }
              _employee = operation.readFromXml();
              int index=0;
              if (order.GetPrimaryKey() == "ssn")
                  index = bs.BS(_employee, _ssn);
              else if (order.GetPrimaryKey() == "salary")
                  index = bs.BS(_employee, _salary);
              else if (order.GetPrimaryKey() == "job")
                  index = bs.BS(_employee, _job);
              else if (order.GetPrimaryKey() == "name")
                  index = bs.BS(_employee, _name);

              if (index == -1)
              {
                  MessageBox.Show(order.GetPrimaryKey()+" not found in the file !!!"); return;
              }
              if (_name != "")
                  _employee[index].name = _name;
              if (_job != "")
                  _employee[index].job = _job;
              if (_salary != "")
                  _employee[index].salary = _salary;
              if (_gender != "")
                  _employee[index].gender = _gender;
              if (_ssn != "")
                  _employee[index].ssn = _ssn;

              operation.list = _employee;

              if (File.Exists("employee.xml"))
              {
                  File.Delete("employee.xml");
                  operation.writethefirstoperation(_employee[0]);
                  operation.WriteAllInXml();
                  //textBox1.Clear();

              }
              MessageBox.Show("successfly update data!!!");
              metroTextBox1.Clear(); metroTextBox7.Clear(); metroTextBox8.Clear(); metroTextBox10.Clear(); metroTextBox9.Clear(); metroGrid3.Rows.Clear();
          }

          private void metroButton3_Click(object sender, EventArgs e)
          {
              string read = metroTextBox3.Text;
              handle.ssn = read;
              if (handle.DeleteExpectedErrors()) { metroTextBox3.Clear(); return; }
              if (!File.Exists("employee.xml"))
              {
                  metroTextBox3.Clear();
                  MessageBox.Show("File Not Found!!!"); return;
              }
              List<employee> _employee = new List<employee>();
              _employee = operation.readFromXml();
              int index = bs.BS(_employee, read);
              if (index == -1)
              {
                  MessageBox.Show("Not Found!!!"); return;
              }
              _employee.RemoveAt(index);
              operation.list = _employee;
              if (_employee.Count == 0)
              {
                  groupBox3.Show();
                  File.Delete("employee.xml");
              }
              else if (File.Exists("employee.xml"))
              {
                  File.Delete("employee.xml");
                  operation.writethefirstoperation(_employee[0]);
                  operation.WriteAllInXml();
                  metroTextBox3.Clear();
                  groupBox3.Show();
              }
              metroTextBox3.Clear(); metroGrid3.Rows.Clear();
          }

          private void metroButton5_Click(object sender, EventArgs e)
          {
             // int counter = 0;
              string prim = "";

              //take the entered sure that the user doesn't put more than one check
              //then call the function primary key in the class orderingattribute if true then print message called operation done' well
              //else if not true "false" print a message said that this operation well cause deletion of the file OK or Cancel..........
              //if(ok) i well retrive all the information from the file then sort them according to the new attribute...
             
              //else i well do nothing...
              if (metroRadioButton5.Checked)
              {
                  prim = metroRadioButton5.Text;
              }
              else if (metroRadioButton8.Checked)
              {
                  prim = metroRadioButton8.Text;

              }
              if (metroRadioButton7.Checked)
              {
                  prim= metroRadioButton7.Text;

              }
              else if (metroRadioButton6.Checked)
              {
                   prim = metroRadioButton6.Text;
              }
              if (metroRadioButton6.Text == "" || metroRadioButton7.Text == "" || metroRadioButton8.Text == "" || metroRadioButton5.Text == "")
              {
                  MessageBox.Show("you don't Check any one "); return;
              }

              if (File.Exists("employee.xml"))
              {
                  string lastprimarykey = order.GetPrimaryKey();
                  order.SetPrimaryKey(prim);
                  _employee = operation.readFromXml();
                  if (order.UniqueAppearence(_employee))
                  {
                      order.SetPrimaryKey(prim);
                      emp.sort(_employee, order.GetPrimaryKey());
                      File.Delete("employee.xml");
                      operation.writethefirstoperation(_employee[0]);
                      operation.list = _employee;
                      operation.WriteAllInXml();
                      MessageBox.Show("it's a uniques appearence");
                  }
                  else
                  {
                      order.SetPrimaryKey(lastprimarykey);
                      MessageBox.Show("Can't be a primary key as it's repeated"); return;
                  }
              }
              MessageBox.Show("done...now primary key is  " + order.GetPrimaryKey());
              metroLabel2.Text = order.GetPrimaryKey() + " is required ";
              metroLabel1.Text = order.GetPrimaryKey();
              metroLabel3.Text = order.GetPrimaryKey(); metroGrid3.Rows.Clear();
          }

          private void metroButton2_Click(object sender, EventArgs e)
          {
             
              string name = metroTextBox2.Text;
              string ssn = metroTextBox6.Text;
              string gender = "";
              if (metroRadioButton1.Checked)
              {
                  gender = metroRadioButton1.Text;
              }
              else if (metroRadioButton2.Checked)
              {
                  gender = metroRadioButton2.Text;
              }
              string job = metroTextBox4.Text;
              string salary = metroTextBox5.Text;

              handle.name = name; handle.salary = salary; handle.ssn = ssn; handle.job = job; handle.gender = gender;
              if (handle.AddExpectedErrors()) return;
              employee newEmployee = new employee(name, ssn, gender, job, salary);
              if (File.Exists("employee.xml"))
              {
                  List<employee> empo = operation.readFromXml();
                  empo.Add(newEmployee);
                  if (order.UniqueAppearence(empo) == false)
                  {
                      MessageBox.Show("can't add becouse the primary key will repeated "); return;
                  }
              }
              List<employee> list = new List<employee>();
              if (!File.Exists("employee.xml"))
              {
                  operation.writethefirstoperation(newEmployee);
              }
              else
              {
                  list = operation.readFromXml();
                  list.Add(newEmployee);
                  File.Delete("employee.xml");
                  list = emp.sort(list, order.GetPrimaryKey());
                  operation.writethefirstoperation(list[0]);
                  operation.list = list;
                  operation.WriteAllInXml();
              }
              metroTextBox4.Clear();
              metroTextBox2.Clear(); metroTextBox6.Clear(); metroTextBox5.Clear(); 
              MessageBox.Show("Saccessfuly Added!!!");
              metroGrid3.Rows.Clear();
            }

          private void metroGrid1_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
          {
              
          }

          private void metroTextBox1_Click(object sender, EventArgs e)
          {
             
          }

          private void metroTextBox7_Click(object sender, EventArgs e)
          {
          }

          private void metroTextBox11_Click(object sender, EventArgs e)
          {
          }

          private void metroTextBox10_Click(object sender, EventArgs e)
          {
          }

          private void metroTextBox9_Click(object sender, EventArgs e)
          {
          }

          private void metroRadioButton3_CheckedChanged(object sender, EventArgs e)
          {
          }

          private void metroTextBox8_Click(object sender, EventArgs e)
          {
          }

          private void metroTabPage3_Click(object sender, EventArgs e)
          {
          }

          private void metroTabPage5_Click(object sender, EventArgs e)
          {
          }

          private void metroTextBox3_Click(object sender, EventArgs e)
          {
          }

          private void metroRadioButton5_CheckedChanged(object sender, EventArgs e)
          {

          }

          private void metroRadioButton8_CheckedChanged(object sender, EventArgs e)
          {
           
          }

          private void metroRadioButton7_CheckedChanged(object sender, EventArgs e)
          {
           

          }

          private void metroRadioButton6_CheckedChanged(object sender, EventArgs e)
          {
              

          }

          private void metroTextBox2_Click(object sender, EventArgs e)
          {
   
          }

          private void metroTextBox6_Click(object sender, EventArgs e)
          {
           
          }

          private void metroRadioButton1_CheckedChanged(object sender, EventArgs e)
          {
       
          }

          private void metroRadioButton2_CheckedChanged(object sender, EventArgs e)
          {
          }

          private void metroTextBox4_Click(object sender, EventArgs e)
          {
          }

          private void metroTextBox5_Click(object sender, EventArgs e)
          {
            
          }

          private void metroGrid2_CellContentClick(object sender, DataGridViewCellEventArgs e)
          {
             
          }

          private void button1_Click(object sender, EventArgs e)
          {

              if (!File.Exists("employee.xml"))
              {
                  MessageBox.Show("file no found"); return;
              }
              List<employee> _employee = new List<employee>();
              _employee = operation.readFromXml();
              if (_employee.Count == 0)
              {
                  MessageBox.Show("no data in file ");
              }
              for (int i = 0; i < _employee.Count; i++)
              {
                  if (metroGrid3.ColumnCount == 0)
                  {
                      metroGrid3.Columns.Add("Name", "Name");
                      metroGrid3.Columns.Add("SSN", "SSN");
                      metroGrid3.Columns.Add("Job", "Job");
                      metroGrid3.Columns.Add("Gender", "Gender");
                      metroGrid3.Columns.Add("Salary", "Salary");
                  }
                  metroGrid3.Rows.Add(new string[] { _employee[i].name, _employee[i].ssn, _employee[i].job, _employee[i].gender, _employee[i].salary });
                
              }
             
           
          }

          private void metroTabPage7_Click(object sender, EventArgs e)
          {
              

          }

          private void metroGrid3_CellContentClick(object sender, DataGridViewCellEventArgs e)
          {
             
          }

          private void metroButton6_Click(object sender, EventArgs e)
          {
              groupBox1.Hide();
              metroGrid1.Rows.Clear();
          }

          private void groupBox1_Enter(object sender, EventArgs e)
          {
           
          }

          private void pictureBox9_Click(object sender, EventArgs e)
          {
             
          }

          private void groupBox2_Enter(object sender, EventArgs e)
          {
           
          }

          private void metroButton4_Click(object sender, EventArgs e)
          {
                  groupBox3.Hide();
                  
        }
      
          private void groupBox3_Enter(object sender, EventArgs e)
          {
    
          }

          private void metroTabPage4_Click(object sender, EventArgs e)
          {
             
          }

          private void metroButton9_Click(object sender, EventArgs e)
          {
              groupBox2.Hide();
          }

    }
}
