﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;
using System.Windows.Forms;

namespace p
{
    class orderingattribute
    {
        fileoperations op;
        List<employee> _employ;
        //  public string LastPrimaryKey;  // well help in form8 in cancel operation 
       //public orderingattribute()
       // {
       //     File.Delete("primaryKey.txt");
       //     FileStream f = new FileStream("primarykey.txt", FileMode.Append);
       //     StreamWriter sw = new StreamWriter(f);
       //     sw.WriteLine("ssn");
       //     sw.Close();
       //     f.Close();
       // }
        public void SetPrimaryKey(string Key)
        {
            File.Delete("primaryKey.txt");
            FileStream f = new FileStream("primarykey.txt", FileMode.Append);
            StreamWriter sw = new StreamWriter(f);
            sw.WriteLine(Key);
            sw.Close();
            f.Close();
        }
        public string GetPrimaryKey()  // get the primary key from the txt file..
        {
            
            FileStream f = new FileStream("primaryKey.txt", FileMode.Open);
            StreamReader sr = new StreamReader(f);
           string ret= sr.ReadLine();
            sr.Close(); f.Close();
            return ret;
        }
        public bool UniqueAppearence(List<employee> emp)  // check if the primary key the user is unique or not by putting it in the set and compare it's size by the orginal size...
        {
            HashSet<string> set = new HashSet<string>();
            for (int i = 0; i < emp.Count; i++)
            {
                if (GetPrimaryKey().ToLower()== "ssn")
                {
                    //insert in set..
                    set.Add(emp[i].ssn);
                }
                else if (GetPrimaryKey().ToLower() == "job")
                {
                    //insert in set..
                    set.Add(emp[i].job);
                }
                else if (GetPrimaryKey().ToLower() == "name")
                {
                    //insert in set...
                    set.Add(emp[i].name);
                }
                else if (GetPrimaryKey().ToLower() == "salary")
                {
                    //insert in set..
                    set.Add(emp[i].salary);
                }
            }
            //check if set is equall the list of employee that the primary key is not repeated..return true else return false;
            if (set.Count == emp.Count)
                return true;
            return false;
        }
     
     
    }
}
