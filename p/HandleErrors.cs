﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;
using System.Windows.Forms;

namespace p
{
    public class HandleErrors  // class all it's rule is to handle the expected error made the user say if the user doesn't fill all the information and so on..
    {
        public string name;
        public string salary;
        public string ssn;
        public string job;
        public string gender;
        public bool ContainNumbers(string input)
        {
            return (input.Where(x => Char.IsDigit(x)).Any());

        }

        public bool AddExpectedErrors() // state that if the user enter as example salary contains letters or name contains number and so on...
        {
            if (ContainNumbers(name))
            {
                MessageBox.Show("Error..Name Should Contains Letters only"); return true;
            }
            if (!ssn.All(char.IsDigit))
            {
                MessageBox.Show("Ssn must Contains Digits only"); return true;
            }
            if (ContainNumbers(job))
            {
                MessageBox.Show("Error..Job Should Contains Letters only"); return true;
            }
            if (!salary.All(char.IsDigit))
            {
                MessageBox.Show("Salary must Contains Digits only"); return true;
            }
            if (job == "" || salary == "" || gender == "" || ssn == "" || name == "") //handle an error expected that if the user not entered the full information of employee..
            {
                MessageBox.Show("You Must Enter All The Data");
                return true;
            }
            return false;
        }
        public bool SearchExpectedErrors()
        {
            if (ssn == "")
            {
                MessageBox.Show("No entered ssn to search for"); return true;
            }
            if (!ssn.All(char.IsDigit))
            {
                MessageBox.Show("Ssn must Contains Digits only"); return true;
            }
            return false;
        }
        public bool DeleteExpectedErrors()
        {
            if (ssn == "")
            {
                MessageBox.Show("No entered SSn To Delete"); return true;
            }

            return false;
        }
        public bool UpdataExpectedErrors()
        {
            if (name == "" && job == "" && salary == "" && gender == "")
            {
                MessageBox.Show("no entered date to update");
                return true;
            }
            if (ContainNumbers(name))
            {
                MessageBox.Show("Error..Name Should Contains Letters only"); return true;
            }
            if (ssn == "")
            {
                MessageBox.Show("you must enter SSn");
                return true;
            }
            if (!ssn.All(char.IsDigit))
            {
                MessageBox.Show("Ssn must Contains Digits only");
                return true;
            }
            if (!File.Exists("employee.xml"))
            {
                MessageBox.Show("Can't found the File");
                return true;
            }
            if (!salary.All(char.IsDigit))
            {
                MessageBox.Show("Salary must Contains Digits only"); return true;
            }
            return false;
        }
    }
}
