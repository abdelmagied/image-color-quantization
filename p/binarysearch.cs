﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace p
{
    public class binarysearch
    {

        orderingattribute order = new orderingattribute();
        employee em = new employee();
        public int index;
        public int BS(List<employee> _employ, string value)  // binary search by the SSN;
        {

            int start = 0; int end = _employ.Count - 1;
            while (start <= end)
            {
                int mid = start + (end - start) / 2;
                if (order.GetPrimaryKey().ToLower() == "ssn" || order.GetPrimaryKey().ToLower() == "salary")  // acting ssn and salary as integer
                {
                    if (int.Parse(_employ[mid].getvalue())==int.Parse(value))
                    {
                        return mid;
                    }
                    else if (int.Parse(_employ[mid].getvalue())>int.Parse((value)))
                    {
                        end = mid - 1;
                    }
                    else if (int.Parse(_employ[mid].getvalue())<int.Parse(value))
                    {
                        start = mid + 1;
                    }
                }
                else//rest use them as string and compare between strings..
                {
                    if ((_employ[mid].getvalue()).CompareTo((value)) == 0)
                    {
                        return mid;
                    }
                    else if ((_employ[mid].getvalue()).CompareTo((value)) == -1)
                    {
                        end = mid - 1;
                    }
                    else if ((_employ[mid].getvalue()).CompareTo(value) == 1)
                    {
                        start = mid + 1;
                    }
                }
              }
            return -1;// then the value not found in the _employee lst

        }

    }
}
