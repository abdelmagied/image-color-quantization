﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.Windows.Forms;
namespace p
{
    public class fileoperations
    {
        public int counter = 0;
        employee em = new employee();
        public employee empoly;
        public List<employee> list;
        public List<employee> EmployeeList;
        public void writethefirstoperation(employee emp)  //writing in the first element in the file to be able to create the file only one..
        {
            XmlWriter writer = XmlWriter.Create("employee.xml");

            writer.WriteStartDocument();
            writer.WriteStartElement("Employee");

            writer.WriteStartElement("Name");
            writer.WriteString(emp.name);
            writer.WriteEndElement();

            writer.WriteStartElement("SSN");
            writer.WriteString(emp.ssn);
            writer.WriteEndElement();

            writer.WriteStartElement("Gender");
            writer.WriteString(emp.gender);
            writer.WriteEndElement();

            writer.WriteStartElement("Job");
            writer.WriteString(emp.job);
            writer.WriteEndElement();

            writer.WriteStartElement("Salary");
            writer.WriteString(emp.salary);
            writer.WriteEndElement();

            writer.WriteEndElement();
            writer.WriteEndDocument();
            writer.Close();

        }
        public void writeInXml(employee emp)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load("employee.xml");

            XmlElement employee = doc.CreateElement("Employee");
            XmlElement node = doc.CreateElement("Name");
            node.InnerText = emp.name;
            employee.AppendChild(node);

            node = doc.CreateElement("SSN");
            node.InnerText = emp.ssn;
            employee.AppendChild(node);

            node = doc.CreateElement("Gender");
            node.InnerText = emp.gender;
            employee.AppendChild(node);

            node = doc.CreateElement("Job");
            node.InnerText = emp.job;
            employee.AppendChild(node);

            node = doc.CreateElement("Salary");
            node.InnerText = emp.salary;
            employee.AppendChild(node);


            XmlElement root = doc.DocumentElement;

            root.AppendChild(employee);
            doc.Save("employee.xml");
        }
        public void WriteAllInXml()  //make for loop and write all the list in the file...by calling the function writeinxml..
        {
            for (int i = 1; i < list.Count; i++)
            {
                writeInXml(list[i]);
            }
        }
        public List<employee> readFromXml() //reading from the xml file...
        {
            XmlDocument doc = new XmlDocument();
            doc.Load("employee.xml");
            XmlNodeList list = doc.GetElementsByTagName("Employee");
            List<employee> EmployeeList = new List<employee>();

            for (int i = 0; i < list.Count; i++)
            {
                XmlNodeList childerns = list[i].ChildNodes;

                string name = childerns[0].InnerText;
                string SSN = childerns[1].InnerText;
                string gender = childerns[2].InnerText;
                string job = childerns[3].InnerText;
                string salary = childerns[4].InnerText;

                employee emp = new employee(name, SSN, gender, job, salary);
                EmployeeList.Add(emp);
            }

            return EmployeeList;

        }
    }
}
