﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.Windows.Forms;
namespace p
{
    [XmlRoot("Employee")]
    public class employee
    {
        [XmlAttribute("Name")]
        public string name { get; set; }
        [XmlAttribute("SSN")]
        public string ssn { get; set; }
        [XmlAttribute("Gender")]
        public string gender { get; set; }
        [XmlAttribute("Job")]
        public string job { get; set; }
        [XmlAttribute("Salary")]
        public string salary { get; set; }
        public int x { get; set; }
        orderingattribute order = new orderingattribute();
        public employee(string name, string ssn, string gender, string job, string salary)
        {
            this.name = name;
            this.ssn = ssn;
            this.gender = gender;
            this.job = job;
            this.salary = salary;

        }
        public string getvalue()
        {
            if (order.GetPrimaryKey().ToLower() == "ssn") { x = 1; return (this.ssn); }
            else if (order.GetPrimaryKey().ToLower() == "salary") { x = 1; return (this.salary); }
            else if (order.GetPrimaryKey().ToLower() == "job"){x=0;return (this.job);}
            else if (order.GetPrimaryKey().ToLower() == "name"){x=0; return (this.name);}
            else
                return "error accure";

        } 
        public List<employee> sort(List<employee> EmployeeList, string orderingattribute) // sort by the key entered by the user (name, ssn, gender, job, salary); in  ascending order
        {
            for (int i = 0; i < EmployeeList.Count; i++)
            {
                for (int j = i + 1; j < EmployeeList.Count; j++)
                {
                    if (order.GetPrimaryKey().ToLower()=="ssn" || order.GetPrimaryKey().ToLower()=="salary") // as in binary search..
                    {
                         if (int.Parse(EmployeeList[i].getvalue())>int.Parse(EmployeeList[j].getvalue()))
                        {
                            employee em;  // swap the 2 object 
                            em = EmployeeList[i];
                            EmployeeList[i] = EmployeeList[j];
                            EmployeeList[j] = em;
                        }
                    }
                    else
                    {
                        if ((EmployeeList[i].getvalue()).CompareTo(EmployeeList[j].getvalue()) == 1)
                        {
                            employee em;  // swap the 2 object 
                            em = EmployeeList[i];
                            EmployeeList[i] = EmployeeList[j];
                            EmployeeList[j] = em;
                        }
                    }

                }
            }
            return EmployeeList;

        }
        public employee() { }

    }
}
